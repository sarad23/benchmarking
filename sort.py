import datetime
import time

starttime = datetime.datetime.now().timestamp()
arr = [3,4,1,3,5,1,92,2,4124,424,52,12]
n = len(arr)


for k in range(1000000):
    for i in range(n):
        for j in range(0, n-i-1):
             if arr[j] > arr[j+1] :
                arr[j], arr[j+1] = arr[j+1], arr[j]

print(arr)

endtime = datetime.datetime.now().timestamp()
# print(starttime)
# print(endtime)
print(endtime - starttime)

